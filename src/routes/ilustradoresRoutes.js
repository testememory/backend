const express = require('express');
const { deleteIlustrador } = require('../controllers/ilustradoresController');
const { updateIlustrador } = require('../controllers/ilustradoresController');
const { insertIlustrador } = require('../controllers/ilustradoresController');
const { selectIlustrador } = require('../controllers/ilustradoresController');
const { listIlustradores } = require('../controllers/ilustradoresController');
const ilustradoresRouter = express.Router();

ilustradoresRouter.get('/', listIlustradores);
ilustradoresRouter.get('/:id', selectIlustrador);
ilustradoresRouter.post('/', insertIlustrador);
ilustradoresRouter.delete('/:id', deleteIlustrador);
ilustradoresRouter.patch('/:id', updateIlustrador);

module.exports = ilustradoresRouter;