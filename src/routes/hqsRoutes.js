const express = require('express');
const { deleteHq } = require('../controllers/hqsController');
const { selectAutorId } = require('../controllers/hqsController');
const { updateHq } = require('../controllers/hqsController');
const { insertHqs } = require('../controllers/hqsController');
const { selectHq } = require('../controllers/hqsController');
const { listHqs } = require('../controllers/hqsController');
const { selectIlustrador } = require('../controllers/ilustradoresController');
const hqsRouter = express.Router();

hqsRouter.get('/', listHqs);
hqsRouter.get('/:id', selectHq);
hqsRouter.get('/autor/:id', selectAutorId);
hqsRouter.get('/ilustrador/:id', selectIlustrador);
hqsRouter.post('/', insertHqs);
hqsRouter.delete('/:id', deleteHq);
hqsRouter.patch('/:id', updateHq);

module.exports = hqsRouter;