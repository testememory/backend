const express = require('express');
const { deleteAutor } = require('../controllers/autoresController');
const { updateAutor } = require('../controllers/autoresController');
const { insertAutores } = require('../controllers/autoresController');
const { selectAutor } = require('../controllers/autoresController');
const { listAutores } = require('../controllers/autoresController');
const autoresRouter = express.Router();

autoresRouter.get('/', listAutores);
autoresRouter.get('/:id', selectAutor);
autoresRouter.post('/', insertAutores);
autoresRouter.delete('/:id', deleteAutor);
autoresRouter.patch('/:id', updateAutor);

module.exports = autoresRouter;