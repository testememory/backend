const { listHqs, selectHq, insertHqs, deleteHq, updateHq, selectIlustradorId, selectAutorId } = require("../models/hqsModel");

module.exports.listHqs = (req, res, next) => {
    listHqs()
        .then(hqs => {
            res.json(hqs);
            next();
        })
        .catch(err => console.log(err));
}

module.exports.insertHqs = (req, res, next) => {
    const body = req.body;
    insertHqs(body.titulo, body.preco, body.genero, body.id_autor, body.id_ilustrador)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.selectHq = (req, res, next) => {
    const id = Number(req.params.id);
    selectHq(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.selectAutorId = (req, res, next) => {
    const id = Number(req.params.id);
    selectAutorId(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.selectIlustradorId = (req, res, next) => {
    const id = Number(req.params.id);
    selectIlustradorId(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.deleteHq = (req, res, next) => {
    const id = Number(req.params.id);
    deleteHq(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.updateHq = (req, res, next) => {
    const body = req.body;
    const id = Number(req.params.id);
    updateHq(body.titulo, body.preco, body.genero, body.id_autor, body.id_ilustrador, id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}