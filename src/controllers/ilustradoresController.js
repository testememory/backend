const { listIlustradores, selectIlustrador, insertIlustrador, deleteIlustrador, updateIlustrador } = require("../models/ilustradoresModel");

module.exports.listIlustradores = (req, res, next) => {
    listIlustradores()
        .then(ilustradores => {
            res.json(ilustradores);
            next();
        })
        .catch(err => console.log(err));
}

module.exports.insertIlustrador = (req, res, next) => {
    const body = req.body;
    insertIlustrador(body.nome, body.idade, body.obras)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.selectIlustrador = (req, res, next) => {
    const id = Number(req.params.id);
    selectIlustrador(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.deleteIlustrador = (req, res, next) => {
    const id = Number(req.params.id);
    deleteIlustrador(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.updateIlustrador = (req, res, next) => {
    const body = req.body;
    const id = Number(req.params.id);
    updateIlustrador(body.nome, body.idade, body.obras, id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}