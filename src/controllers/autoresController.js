const { listAutores, selectAutor, insertAutores, deleteAutor, updateAutor } = require("../models/autoresModel");

module.exports.listAutores = (req, res, next) => {
    listAutores()
        .then(autores => {
            res.json(autores);
            next();
        })
        .catch(err => console.log(err));
}

module.exports.insertAutores = (req, res, next) => {
    const body = req.body;
    insertAutores(body.nome, body.idade, body.obras)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.selectAutor = (req, res, next) => {
    const id = Number(req.params.id);
    selectAutor(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.deleteAutor = (req, res, next) => {
    const id = Number(req.params.id);
    deleteAutor(id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}

module.exports.updateAutor = (req, res, next) => {
    const body = req.body;
    const id = Number(req.params.id);
    updateAutor(body.nome, body.idade, body.obras, id)
        .then(response => {
            res.json({response});
            next();
        })
        .catch(err => console.log(err));
}