const { dbQuery } = require("../db");

module.exports.listAutores = async () => {
    const retorno = await dbQuery(`SELECT * FROM autores`, []); 
    return retorno;
}

module.exports.insertAutores = async (nome, idade, obras) => {
    await dbQuery(`INSERT INTO autores (nome, idade, obras) VALUES('${nome}', ${idade}, '${obras}')`);
    return 'Registro criado';
}

module.exports.selectAutor = async (id) => {
    const retorno = await dbQuery(`SELECT * FROM autores WHERE id = ${id}`);
    return retorno;
}

module.exports.deleteAutor = async (id) => {
    await dbQuery(`DELETE FROM autores WHERE id = ${id}`);
    return `Registro deletado`; 
}

module.exports.updateAutor = async (nome, idade, obras, id) => {
    await dbQuery(`UPDATE autores SET nome = '${nome}', idade = ${idade}, obras = '${obras}' WHERE id = ${id}`);
    return 'Registro atualizado';
}