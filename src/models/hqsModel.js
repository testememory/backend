const { dbQuery } = require("../db");

module.exports.listHqs = async () => {
    const retorno = await dbQuery(`SELECT * FROM hqs`, []); 
    return retorno;
}

module.exports.insertHqs = async (titulo, preco, genero, id_autor, id_ilustrador) => {
    await dbQuery(`INSERT INTO hqs (titulo, preco, genero, id_autor, id_ilustrador) VALUES('${titulo}', ${preco}, '${genero}', ${id_autor}, ${id_ilustrador})`);
    return 'Registro criado';
}

module.exports.selectHq = async (id) => {
    const retorno = await dbQuery(`SELECT * FROM hqs WHERE id = ${id}`);
    return retorno;
}

module.exports.selectAutorId = async (id_autor) => {
    const retorno = await dbQuery(`SELECT autores.* FROM hqs
    JOIN autores ON hqs.id_autor = autores.id
    WHERE autores.id = ${id_autor}`);
    return retorno;
}

module.exports.selectIlustradorId = async (id_ilustrador) => {
    const retorno = await dbQuery(`SELECT ilustradores.* FROM hqs
    JOIN ilustradores ON hqs.id_ilustrador = ilustradores.id
    WHERE ilustradores.id = ${id_ilustrador}`);
    return retorno;
}

module.exports.deleteHq = async (id) => {
    await dbQuery(`DELETE FROM hqs WHERE id = ${id}`);
    return `Registro deletado`; 
}

module.exports.updateHq = async (titulo, preco, genero, id_autor, id_ilustrador, id) => {
    await dbQuery(`UPDATE hqs SET titulo = '${titulo}', preco = ${preco}, genero = '${genero}', id_autor = ${id_autor}, id_ilustrador = ${id_ilustrador} WHERE id = ${id}`);
    return 'Registro atualizado';
}