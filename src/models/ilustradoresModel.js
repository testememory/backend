const { dbQuery } = require("../db");

module.exports.listIlustradores = async () => {
    const retorno = await dbQuery(`SELECT * FROM ilustradores`, []); 
    return retorno;
}

module.exports.insertIlustrador = async (nome, idade, obras) => {
    await dbQuery(`INSERT INTO ilustradores (nome, idade, obras) VALUES ('${nome}', ${idade}, '${obras}')`);
    return 'Registro criado';
}

module.exports.selectIlustrador = async (id) => {
    const retorno = await dbQuery(`SELECT * FROM ilustradores WHERE id = ${id}`);
    return retorno;
}

module.exports.deleteIlustrador = async (id) => {
    await dbQuery(`DELETE FROM ilustradores WHERE id = ${id}`);
    return `Registro deletado`; 
}

module.exports.updateIlustrador = async (nome, idade, obras, id) => {
    await dbQuery(`UPDATE ilustradores SET nome = '${nome}', idade = ${idade}, obras = '${obras}' WHERE id = ${id}`);
    return 'Registro atualizado';
}