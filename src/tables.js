const { openConnection } = require("./db");
const db =  openConnection();

module.exports.createTables = async() => {
    await db.run(`CREATE TABLE IF NOT EXISTS ilustradores (id INTEGER PRIMARY KEY AUTOINCREMENT,nome STRING, idade INTEGER, obras STRING)`, (err) => {
        if(err) {
            console.log(err);
        }
        else {
            console.log("Tabela ilustradores criada");
        }
    })
    await db.run(`CREATE TABLE IF NOT EXISTS autores (id INTEGER PRIMARY KEY AUTOINCREMENT,nome STRING, idade INTEGER, obras STRING)`, (err) => {
        if(err) {
            console.log(err);
        }
        else {
            console.log("Tabela autores criada");
        }
    })
    await db.run(`CREATE TABLE IF NOT EXISTS hqs (id INTEGER PRIMARY KEY AUTOINCREMENT,titulo STRING, preco FLOAT, genero STRING, id_autor INTEGER, id_ilustrador INTEGER)`, (err) => {
        if(err) {
            console.log(err);
        }
        else {
            console.log("Tabela hqs criada");
        }
    })
    await db.close(() => {
        console.log("Conexão fechada");
    });
}