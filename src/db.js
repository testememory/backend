const sqlite3 = require('sqlite3').verbose();

// Estabelece conexão com BD
module.exports.openConnection = () => {
    const dbPath = `${__dirname}/hqs.db`;
    const db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
            console.error(err);
        }
        else console.log("Sucesso conexão");
    });
    return db;
}

// Abre conexão, executa query e fecha conexão
module.exports.dbQuery = (query, params) => {
    let db = this.openConnection();
    return new Promise((resolve, reject) => {
        db.all(query, params, (err, rows) => {
            if(err)
                reject(err);
            else
                resolve(rows);
        })
    })
    .finally(() => {
        db.close();
        console.log("Conexão fechada");
    })
}