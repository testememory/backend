var express = require('express');
var app = express();

const bodyParser = require('body-parser');
const cors = require('cors');

const ilustradoresRouter = require('./src/routes/ilustradoresRoutes');
const autoresRouter = require('./src/routes/autoresRoutes');
const hqsRouter = require('./src/routes/hqsRoutes');
const { createTables } = require('./src/tables');

// Rota de teste
app.get('/', function(req, res) {
  res.send('Termina hoje?');
});

app.listen(3000);

// Para uso de rotas
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

//Cria tabelas
createTables();

// Rotas
app.use('/api/ilustradores', ilustradoresRouter);
app.use('/api/autores', autoresRouter);
app.use('/api/hqs', hqsRouter);

module.exports = app;